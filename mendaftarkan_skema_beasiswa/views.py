from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.db import connection
from .forms import PaketBeasiswa_Form, Beasiswa_Form
from .models import PaketBeasiswa, Beasiswa
from tentukan_status_calon.models import SkemaBeasiswa, SkemaBeasiswaAktif
import datetime
# Create your views here.
response = {}
cursor = connection.cursor()
def index(request):
	if("username" not in request.session):
		return HttpResponseRedirect(reverse('login_logout:index'))
	else:
		username = request.session['username']
		response['username'] = username
		query = "SELECT * FROM pengguna WHERE username='"+username+"'"
		cursor.execute(query)
		select = dictfetchall(cursor)
		print(select[0]['role']) #ini cetak donatur
		# cek dia donatur atau bukan(!IMPORTANT)

		role = select[0]['role']
		if role == 'donatur':
			response['allowed'] = True
			response['donatur'] = True
			response['mahasiswa'] = False
		elif role == 'mahasiswa':
			response['allowed'] = False
			response['mahasiswa'] = True
			response['donatur'] = False
		else :
			response['allowed'] = False
			response['mahasiswa'] = False
			response['donatur'] = False
		return render(request, 'mendaftarkan_skema_beasiswa/index.html', response)

def pendaftaran_beasiswa(request):
	response['kode_double'] = False
	response['beasiswaBaru_form'] = PaketBeasiswa_Form
	html = 	'mendaftarkan_skema_beasiswa/beasiswaBaru.html'
	return render (request, html, response)

def pendaftaran_beasiswa_dari_paket(request):
	cursor.execute('SELECT kode from simbion.skema_beasiswa')
	response['kode']=dictfetchall(cursor)
	response['not_unique'] = False
	response['beasiswa_form'] = Beasiswa_Form
	html = 	'mendaftarkan_skema_beasiswa/beasiswaDariPaket.html'
	return render (request, html, response)

def daftarkan_beasiswa(request):
	form = PaketBeasiswa_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['kode'] = request.POST['kode']
		cursor.execute("SELECT * from simbion.skema_beasiswa where kode='"+response['kode']+"'")
		select = cursor.fetchone()
		if(select is not None):
			print("KEMBAR")
			response['kode_double'] = True
			response['beasiswaBaru_form'] = PaketBeasiswa_Form
			html ='mendaftarkan_skema_beasiswa/beasiswaBaru.html'
			return render(request, html, response)
		else:
			print('GAKEMBAR')
			response['nama_paket_beasiswa'] = request.POST['nama_paket_beasiswa']
			response['jenis_paket_beasiswa'] = request.POST['jenis_paket_beasiswa']
			response['syarat_beasiswa'] = request.POST['syarat_beasiswa']
			response['deskripsi'] = request.POST['deskripsi']

			username = request.session['username']
			query = "SELECT * FROM donatur WHERE username='"+username+"'"
			cursor.execute(query)
			select = dictfetchall(cursor)
			print(select[0]['nomor_identitas'])
			response['no_identitas_donatur'] = select[0]['nomor_identitas']

			query = "INSERT INTO SKEMA_BEASISWA(kode, nama, jenis, deskripsi, no_identitas_donatur) VALUES ("+response['kode']+", '"+response['nama_paket_beasiswa']+"', '"+response['jenis_paket_beasiswa']+"', '"+response['deskripsi']+"', "+response['no_identitas_donatur']+")"
			cursor.execute(query)

			query = "INSERT INTO SYARAT_BEASISWA(kode_beasiswa, syarat) VALUES ("+response['kode']+", '"+response['syarat_beasiswa']+"')"
			cursor.execute(query)

			return HttpResponseRedirect('/daftarkan_beasiswa/')
	else:
		return HttpResponseRedirect('/daftarkan_beasiswa/')


def daftarkan_beasiswa_dari_paket(request):
	form = Beasiswa_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['kode_beasiswa'] = request.POST['kode_beasiswa']
		response['nomor_urut'] = request.POST['nomor_urut']
		response['tanggal_mulai_pendaftaran'] = request.POST['tanggal_mulai_pendaftaran']
		response['tanggal_tutup_pendaftaran'] = request.POST['tanggal_tutup_pendaftaran']

		cursor.execute("SELECT * from simbion.skema_beasiswa_aktif where kode_skema_beasiswa='"+response['kode_beasiswa']+"' AND no_urut='"+response['nomor_urut']+"'")
		select = cursor.fetchone()
		print(select)
		if(select is not None):
			response['not_unique'] = True
			response['beasiswa_form'] = Beasiswa_Form
			html ='mendaftarkan_skema_beasiswa/beasiswaDariPaket.html'
			return render(request, html, response)
		else:
			periode = response['tanggal_mulai_pendaftaran']+" s/d "+response['tanggal_tutup_pendaftaran']
			status = 'Buka'
			query = "INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa, no_urut, tgl_mulai_pendaftaran, tgl_tutup_pendaftaran, periode_penerimaan, status, jumlah_pendaftar, total_pembayaran) VALUES ("+response['kode_beasiswa']+","+response['nomor_urut']+", '"+response['tanggal_mulai_pendaftaran']+"', '"+response['tanggal_tutup_pendaftaran']+"', '"+periode+"', '"+status+"', "+'0'+','+'0'+")"
			cursor.execute(query)

			return HttpResponseRedirect('/daftarkan_beasiswa/')
	else:
		return HttpResponseRedirect('/daftarkan_beasiswa/')

def dictfetchall(cursor):
	"Return all rows from a cursor as a dict"
	columns = [col[0] for col in cursor.description]
	return [
		dict(zip(columns, row))
		for row in cursor.fetchall()
	]