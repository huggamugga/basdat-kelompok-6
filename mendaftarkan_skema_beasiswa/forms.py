from django import forms
from .models import PaketBeasiswa
from django.utils.translation import ugettext as _

class PaketBeasiswa_Form(forms.Form):
	error = {
		'required' : 'Please fill in the blank'
	}
	attrs = {
		'class' : 'form-control'
	}
	CHOICES = (('Prestasi', 'Prestasi'),('Ekonomi', 'Ekonomi'),('Tugas Akhir', 'Tugas Akhir'))
	kode = forms.IntegerField(widget=forms.TextInput(attrs=attrs),required=True)
	nama_paket_beasiswa = forms.CharField(max_length = 100, widget=forms.TextInput(attrs=attrs), required=True)
	jenis_paket_beasiswa = forms.ChoiceField(choices=CHOICES, widget=forms.Select(attrs=attrs), required=True)
	deskripsi = forms.CharField(widget=forms.Textarea(attrs=attrs), required=True)
	syarat_beasiswa = forms.CharField(widget=forms.Textarea(attrs=attrs), required=True)

class Beasiswa_Form(forms.Form):
	error = {
		'required' : 'Please fill in the blank'
	}
	attrs = {
		'class' : 'form-control'
	}
	# kode_beasiswa = forms.ModelChoiceField(widget=forms.Select(attrs=attrs), queryset=PaketBeasiswa.objects.values('kode'))
	nomor_urut = forms.IntegerField(widget=forms.TextInput(attrs=attrs),required=True)
	# tanggal_mulai_pendaftaran = forms.DateField(initial='YYYY-DD-MM', widget=forms.DateInput(attrs=attrs), required=True)
	# tanggal_tutup_pendaftaran = forms.DateField(initial='YYYY-DD-MM', widget=forms.DateInput(attrs=attrs), required=True)