from django.apps import AppConfig


class MendaftarkanSkemaBeasiswaConfig(AppConfig):
    name = 'mendaftarkan_skema_beasiswa'
