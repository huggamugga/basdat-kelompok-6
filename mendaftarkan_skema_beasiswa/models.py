from django.db import models
import datetime
# Create your models here.
class PaketBeasiswa(models.Model):
	kode = models.IntegerField(primary_key = True)
	nama_paket_beasiswa = models.CharField(max_length = 100, blank = False)
	jenis_paket_beasiswa = models.CharField(max_length = 20, blank = False)
	syarat_beasiswa = models.TextField(blank = False)

class Beasiswa(models.Model):
	kode_beasiswa = models.IntegerField(blank = False)
	nomor_urut = models.IntegerField(blank = False)
	tanggal_mulai_pendaftaran = models.DateField(auto_now = False, auto_now_add = False, blank = False, default=datetime.date.today)
	tanggal_tutup_pendaftaran = models.DateField(auto_now = False, auto_now_add = False, blank = False, default=datetime.date.today)

	class Meta:
		unique_together = ('kode_beasiswa', 'nomor_urut')