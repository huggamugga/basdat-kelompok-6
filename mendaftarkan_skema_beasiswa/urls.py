from django.conf.urls import url
from .views import index, daftarkan_beasiswa, pendaftaran_beasiswa, pendaftaran_beasiswa_dari_paket, daftarkan_beasiswa_dari_paket

#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^beasiswa_baru', daftarkan_beasiswa, name='beasiswa_baru'),
    url(r'^daftar', pendaftaran_beasiswa, name='pendaftaran_beasiswa'),
    url(r'^paket_beasiswa', daftarkan_beasiswa_dari_paket, name='paket_beasiswa'),
    url(r'^tambah', pendaftaran_beasiswa_dari_paket, name='beasiswa_dari_paket'),
]