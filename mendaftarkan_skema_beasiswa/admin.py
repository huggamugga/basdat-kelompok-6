from django.contrib import admin

# Register your models here.
from .models import Beasiswa, PaketBeasiswa
from tentukan_status_calon.models import *

admin.site.register(Beasiswa)
admin.site.register(PaketBeasiswa)
admin.site.register(SkemaBeasiswa)
admin.site.register(Pengguna)
admin.site.register(Pembayaran)
admin.site.register(Donatur)
admin.site.register(Admin)
admin.site.register(IndividualDonor)
admin.site.register(Yayasan)
admin.site.register(Mahasiswa)
admin.site.register(RiwayatAkademik)
admin.site.register(SyaratBeasiswa)
admin.site.register(SkemaBeasiswaAktif)
admin.site.register(Pendaftaran)
admin.site.register(TempatWawancara)
admin.site.register(Wawancara)
admin.site.register(Pengumuman)