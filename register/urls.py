from django.conf.urls import url
from register.views import index, form_mhs, form_dp, form_dy, kategori \
    , reg_mhs, reg_dp, reg_dy
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^kategori/(?P<kategori>\d+)/$',kategori,name="kategori"),
    url(r'^form_mhs/$',form_mhs,name='form_mhs'),
    url(r'^form_dp/$',form_dp,name='form_dp'),
    url(r'^form_dy/$',form_dy,name='form_dy'),
    url(r'^reg_mhs/$',reg_mhs,name='reg_mhs'),
    url(r'^reg_dp/$',reg_dp,name='reg_dp'),
    url(r'^reg_dy/$',reg_dy,name='reg_dy'),
]