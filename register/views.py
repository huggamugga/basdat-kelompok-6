from django.shortcuts import render
from mendaftarkan_skema_beasiswa.models import Beasiswa
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.db import connection, connections


# Create your views here.
response = {}
def index(request):
    if("username" not in request.session):
        return render(request, 'register/index.html', response)
    else:
        return HttpResponseRedirect(reverse('melihat_informasi_beasiswa_aktif:index'))

def kategori(request, kategori):
    if(kategori == '1'):
        return HttpResponseRedirect(reverse('register:form_mhs'))
    elif(kategori == '2'):
        return HttpResponseRedirect(reverse('register:form_dp'))
    elif(kategori == '3'):
        return HttpResponseRedirect(reverse('register:form_dy'))

def form_mhs(request):
    return render(request, 'register/register_mahasiswa.html',response)

def form_dp(request):
    return render(request, 'register/register_donatur_pribadi.html',response)

def form_dy(request):
    return render(request, 'register/register_donatur_yayasan.html', response)

def reg_mhs(request):
    reg_pengguna(request.POST["username"],request.POST["password"],"mahasiswa")

    query = "INSERT INTO MAHASISWA(npm, email, nama, no_telp, alamat_tinggal, alamat_domisili, nama_bank, no_rekening, nama_pemilik, username) VALUES "+\
            "(" + request.POST["npm"] + ", '" + request.POST["email"] + "', '" + request.POST["nama_lengkap"] + "', " + request.POST["no_telp"] + ", '" + request.POST["alamat_t"] + "', '" +\
            request.POST["alamat_d"] + "', '" + request.POST["instansi_bank"] + "', " + request.POST["no_rek"] + ", '" + request.POST["nama_pemilik"] + "', '" + request.POST["username"] + "');"
    cursor = connection.cursor()
    cursor.execute(query)

    return HttpResponseRedirect(reverse('login:index'))

def reg_dp(request):
    reg_pengguna(request.POST["username"],request.POST["password"],"donatur")
    reg_donatur(request.POST["no_id"],request.POST["email"],request.POST["nama_lengkap"],request.POST["npwp"],
                request.POST["no_telp"],request.POST["alamat"],request.POST["username"])


    query = "INSERT INTO INDIVIDUAL_DONOR(nik, nomor_identitas_donatur) VALUES "+\
            "(" + request.POST["nik"] + "," + request.POST["no_id"] + ");"
    cursor = connection.cursor()
    cursor.execute(query)

    return HttpResponseRedirect(reverse('login:index'))

def reg_dy(request):
    reg_pengguna(request.POST["username"],request.POST["password"],"donatur")
    reg_donatur(request.POST["no_id"],request.POST["email"],request.POST["nama_cp"],request.POST["npwp"],
                request.POST["no_telp"],request.POST["alamat"],request.POST["username"])


    query = "INSERT INTO YAYASAN(no_sk_yayasan, email, nama, no_telp_cp, nomor_identitas_donatur) VALUES "+\
            "('" + request.POST["no_sk"] + "', '" + request.POST["email"] + "', '" + request.POST["nama_yayasan"] + "', " + request.POST["no_telp"] + ", " + request.POST["no_id"] + ");"
    cursor = connection.cursor()
    cursor.execute(query)


    return HttpResponseRedirect(reverse('login:index'))

def reg_pengguna(username, password, role):
    query = "INSERT INTO PENGGUNA(username, password, role) VALUES "+\
            "('" + username + "', '" + password + "', '" + role +"');"
    cursor = connection.cursor()
    cursor.execute(query)

def reg_donatur(no_id,email,nama,npwp,no_telp,alamat,username):
    query = "INSERT INTO DONATUR(nomor_identitas, email, nama, npwp, no_telp, alamat, username) VALUES "+\
            "(" + no_id + ", '" + email + "', '" + nama + "', " + npwp + ", " + no_telp + \
            ", '" + alamat + "', '" + username + "');"
    cursor = connection.cursor()
    cursor.execute(query)