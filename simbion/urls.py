"""simbion URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import RedirectView
import login_logout.urls as login
import register.urls as register
from login_logout.views import auth_logout
import melihat_informasi_beasiswa_aktif.urls as melihat_informasi_beasiswa_aktif
import mendaftarkan_skema_beasiswa.urls as mendaftarkan_skema_beasiswa
import daftar_calon_penerima_beasiswa.urls as daftar_calon_penerima_beasiswa
import tentukan_status_calon.urls as tentukan_status_calon

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^beasiswa_aktif/', include(('melihat_informasi_beasiswa_aktif.urls','melihat_informasi_beasiswa_aktif'), namespace='melihat-informasi-beasiswa-aktif')),
    url(r'^daftarkan_beasiswa/', include(('mendaftarkan_skema_beasiswa.urls', 'mendaftarkan_skema_beasiswa'), namespace='mendaftarkan-skema-beasiswa')),
    url(r'^$', RedirectView.as_view(url = "/beasiswa_aktif/", permanent = "true"), name = 'index'),
    url(r'^daftar-calon-penerima-beasiswa/', include(('daftar_calon_penerima_beasiswa.urls', 'daftar_calon_penerima_beasiswa'), namespace='daftar-calon-penerima-beasiswa')),
    url(r'^list-beasiswa/', include(('tentukan_status_calon.urls', 'tentukan_status_calon'), namespace='list-beasiswa')),
    url(r'^login/', include(('login_logout.urls', 'login'), namespace="login")),
    url(r'^logout/', auth_logout, name="logout"),
    url(r'^register/', include(('register.urls', 'register'),namespace="register"))
]
