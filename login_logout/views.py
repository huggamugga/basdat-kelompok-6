from django.shortcuts import render
from mendaftarkan_skema_beasiswa.models import Beasiswa
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.db import connection, connections

# Create your views here.
response = {}
def index(request):
    if("username" not in request.session):
        return render(request, 'login_logout/index.html', response)
    else:
        return HttpResponseRedirect(reverse('melihat_informasi_beasiswa_aktif:index'))


def auth_login(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']

        cursor = connection.cursor()
        query = "select username, role from pengguna where username = '" + username + "' and password = '" + password + "'"
        cursor.execute(query)

        result = cursor.fetchone()
        print(result)

        if(result is not None):
            request.session["username"] = result[0]
            request.session["role"] = result[0]

        return HttpResponseRedirect(reverse('login:index'))

def auth_logout(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('melihat_informasi_beasiswa_aktif:index'))
