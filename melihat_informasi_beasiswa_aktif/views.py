from django.shortcuts import render
from django.db import connection
from mendaftarkan_skema_beasiswa.models import Beasiswa
from tentukan_status_calon.models import SkemaBeasiswaAktif
# Create your views here.
response = {}
cursor = connection.cursor()
def index(request):
	if("username" not in request.session):
		response['not_logged_in'] = True
	else:
		response['not_logged_in'] = False
		username = request.session['username']
		response['username'] = username
		query = "SELECT * FROM pengguna WHERE username='"+username+"'"
		cursor.execute(query)
		select = dictfetchall(cursor)
		print(select[0]['role'])

		role = select[0]['role']
		if role == 'donatur':
			response['donatur'] = True
			response['mahasiswa'] = False
		else:
			response['mahasiswa'] = True
			response['donatur'] = False

	cursor.execute("SELECT * FROM skema_beasiswa")
	select = dictfetchall(cursor)
	response['beasiswaAsli'] = select

	cursor.execute("SELECT * FROM skema_beasiswa_aktif")
	select = dictfetchall(cursor)
	response['beasiswa'] = select
	return render(request, 'melihat_informasi_beasiswa_aktif/index.html', response)

def detail_beasiswa(request, kode, no_urut):
	if("username" not in request.session):
		response['not_logged_in'] = True
	else:
		response['not_logged_in'] = False
		username = request.session['username']
		query = "SELECT * FROM pengguna WHERE username='"+username+"'"
		cursor.execute(query)
		select = dictfetchall(cursor)
		print(select[0]['role'])

		role = select[0]['role']
		if role == 'donatur':
			response['donatur'] = True
			response['mahasiswa'] = False
		elif role == 'mahasiswa' :
			response['mahasiswa'] = True
			response['donatur'] = False

	query = "SELECT * FROM skema_beasiswa WHERE kode='"+kode+"'"
	cursor.execute(query)
	select = dictfetchall(cursor)
	response['beasiswa'] = select[0]

	query = "SELECT * FROM skema_beasiswa_aktif WHERE kode_skema_beasiswa='"+kode+"' AND no_urut='"+no_urut+"'"
	cursor.execute(query)
	select = dictfetchall(cursor)
	response['beasiswaAktif'] = select[0]

	query = "SELECT * FROM syarat_beasiswa WHERE kode_beasiswa='"+kode+"'"
	cursor.execute(query)
	select = dictfetchall(cursor)
	response['beasiswaSyarat'] = select[0]
	return render(request, 'melihat_informasi_beasiswa_aktif/detail.html', response)

def dictfetchall(cursor):
	"Return all rows from a cursor as a dict"
	columns = [col[0] for col in cursor.description]
	return [
		dict(zip(columns, row))
		for row in cursor.fetchall()
	]