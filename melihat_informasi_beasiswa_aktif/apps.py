from django.apps import AppConfig


class MelihatInformasiBeasiswaAktifConfig(AppConfig):
    name = 'melihat_informasi_beasiswa_aktif'
