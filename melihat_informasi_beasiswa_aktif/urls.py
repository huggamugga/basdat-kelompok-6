from django.conf.urls import url
from .views import index, detail_beasiswa
from daftar_calon_penerima_beasiswa.views import indexKodeBeasiswa

#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^(?P<kode_beasiswa>.*)/daftar', indexKodeBeasiswa, name='indexKodeBeasiswa' ),
    url(r'^(?P<kode>.*)/(?P<no_urut>.*)', detail_beasiswa, name='detail-beasiswa'),
]