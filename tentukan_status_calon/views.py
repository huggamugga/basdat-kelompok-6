from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.db import connection, connections
from django.urls import reverse

response = {}
# Create your views here.
def index(request, id_donatur=None):
    return render(request, 'tentukan_status_calon/list_beasiswa.html')

def lihatDaftarBeasiswaSaya(request, id_donatur=None):
    if 'username' in request.session:
        cursor = connection.cursor()
        query = "select nomor_identitas, nomor_identitas from donatur where username='%s'" % request.session['username']
        cursor.execute(query)
        row = cursor.fetchall()
        data = row[0][0]

        query1 = "select SBA.no_urut, SBA.kode_skema_beasiswa, SBA.tgl_tutup_pendaftaran, SBA.status, SBA.jumlah_pendaftar from skema_beasiswa_aktif SBA, skema_beasiswa SB where SB.kode=SBA.kode_skema_beasiswa AND SB.no_identitas_donatur = '%s';" % data
        cursor.execute(query1)
        row = cursor.fetchall()
        response['list_beasiswa'] = row

        return render(request, 'tentukan_status_calon/list_beasiswa.html', response)


def untukLihat(request, kode_sb):
    request.session['kode_sb'] = kode_sb
    return HttpResponseRedirect(reverse('tentukan_status_calon:lihat-pendaftar'))

def lihatListPendaftar(request):
    kode_sb = request.session['kode_sb']
    cursor = connection.cursor()
    query1 = "select P.no_urut, M.nama, M.npm, P.waktu_daftar, P.status_terima from pendaftaran P, mahasiswa M where P.npm=M.npm AND P.kode_skema_beasiswa = '%s';" % kode_sb
    cursor.execute(query1)
    row = cursor.fetchall()
    response['list_pendaftar'] = row

    return render(request, 'tentukan_status_calon/daftar_pendaftar.html', response)

def untukUpdateTerima(request, npm):
    request.session['npm'] = npm
    return HttpResponseRedirect(reverse('tentukan_status_calon:update-status-terima'))

def updateStatusTerima(request, npm=None):
    npm = request.session['npm']
    cursor = connection.cursor()
    query = "update pendaftaran set status_terima = 'Aktif' where npm = '%s';" % npm
    cursor.execute(query)
    return HttpResponseRedirect(reverse('tentukan_status_calon:lihat-pendaftar'))

def untukUpdateTolak(request, npm):
    request.session['npm'] = npm
    return HttpResponseRedirect(reverse('tentukan_status_calon:update-status-tolak'))

def updateStatusTolak(request, npm=None):
    npm = request.session['npm']
    cursor = connection.cursor()
    query = "update pendaftaran set status_terima = 'Tidak Aktif' where npm = '%s';" % npm
    cursor.execute(query)
    return HttpResponseRedirect(reverse('tentukan_status_calon:lihat-pendaftar'))
    