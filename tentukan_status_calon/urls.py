from django.conf.urls import url
from .views import index, lihatDaftarBeasiswaSaya, lihatListPendaftar, updateStatusTerima, updateStatusTolak, untukLihat, untukUpdateTerima, untukUpdateTolak
#url for app
urlpatterns = [
    url(r'^$', lihatDaftarBeasiswaSaya, name='index'),
    url(r'^untuk-lihat/(?P<kode_sb>\w+)/$', untukLihat, name='untuk-lihat'),       
    url(r'^lihat-pendaftar/$', lihatListPendaftar, name='lihat-pendaftar'),
    url(r'^untuk-update-terima/(?P<npm>\w+)/$', untukUpdateTerima, name='untuk-update-terima'),
    url(r'^update-status-terima/$', updateStatusTerima, name='update-status-terima'),
    url(r'^untuk-update-tolak/(?P<npm>\w+)/$', untukUpdateTolak, name='untuk-update-tolak'),
    url(r'^update-status-tolak/$', updateStatusTolak, name='update-status-tolak'),
    ]