from django.apps import AppConfig


class DaftarCalonPenerimaBeasiswaConfig(AppConfig):
    name = 'daftar_calon_penerima_beasiswa'
