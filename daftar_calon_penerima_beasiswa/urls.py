from django.conf.urls import url
from .views import index, beasiswa_post

#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^daftar-beasiswa', beasiswa_post, name='daftar-beasiswa'),
]