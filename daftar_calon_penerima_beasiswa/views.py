from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.db import connection, connections
from .forms import *
import datetime

response = {}
# Create your views here.
def index(request):
    if 'username' in request.session:
        cursor = connection.cursor()
        query1 = "select npm, npm from mahasiswa where username = '%s'" % request.session['username']
        cursor.execute(query1)
        row = cursor.fetchall()
        print(row)

        response['formBeasiswa'] = Beasiswa_Form(initial={'NPM':row[0][0]})
        return render(request, 'daftar_calon_penerima_beasiswa/formulir.html', response)

def indexKodeBeasiswa(request, kode_beasiswa=None):
    if 'username' in request.session:
        print(request.session['username'])
        cursor = connection.cursor()
        query1 = "select npm, npm from mahasiswa where username = '%s'" % request.session['username']
        cursor.execute(query1)
        row = cursor.fetchall()
        print(row)
        print(kode_beasiswa)

        response['formBeasiswa'] = Beasiswa_Form(initial={'NPM':row[0][0], 'kode_beasiswa':kode_beasiswa})
        return render(request, 'daftar_calon_penerima_beasiswa/formulir.html', response)

def beasiswa_post(request):
    form = Beasiswa_Form(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        kode_beasiswa = request.POST['kode_beasiswa']
        NPM = request.POST['NPM']
        email = request.POST['email']
        index_prestasi = request.POST['index_prestasi']
        now = datetime.datetime.now()
        nowfinal = '{:%Y-%m-%d %H:%M:%S}'.format(now)

        cursor = connection.cursor()
        query1 = "select * from pendaftaran where npm = '%s' AND kode_skema_beasiswa = '%s';" %(NPM, kode_beasiswa)
        cursor.execute(query1)
        hasil = cursor.fetchall()
        print(hasil)
        if (len(hasil) == 0):
            cursor = connection.cursor()
            query = "insert into pendaftaran values ('%s', '%s', '%s', '%s', '%s', '%s');" % (1, kode_beasiswa, NPM, nowfinal, "Telah Mendaftar", "Proses Seleksi")
            cursor.execute(query)
            return render(request, 'daftar_calon_penerima_beasiswa/suksesdaftar.html', response)
        else :
            return render(request, 'daftar_calon_penerima_beasiswa/gagaldaftar.html', response)