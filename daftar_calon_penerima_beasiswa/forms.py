from django import forms
from django.db import connection, connections
from django.forms import ModelForm


class Beasiswa_Form(forms.Form):
    cursor = connection.cursor()
    query = "select kode_skema_beasiswa, kode_skema_beasiswa from skema_beasiswa_aktif where status = '%s'" % "Buka"
    cursor.execute(query)
    row = cursor.fetchall()

    kode_beasiswa = forms.IntegerField(label='Kode Beasiswa', widget=forms.Select(choices=row, attrs={'class': 'btn btn-primary dropdown-toggle w-100'}))
    NPM = forms.IntegerField(label='NPM', widget=forms.TextInput(attrs={'class': 'btn btn-primary w-100'}))
    email = forms.CharField(label='E-Mail', max_length=50, widget=forms.TextInput(attrs={'class': 'btn btn-primary w-100'}))
    index_prestasi = forms.FloatField(label='Indeks Prestasi', widget=forms.TextInput(attrs={'class': 'btn btn-primary w-100'}))